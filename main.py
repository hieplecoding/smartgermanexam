from dotenv import load_dotenv
import os
import openai
from typing import List
from fastapi import FastAPI, HTTPException, Depends
from fastapi.middleware.cors import CORSMiddleware

# controllers
from controllers.question_controller import multiple_choice_controller
from controllers.checking_essay_controller import checking_essay_controller
from controllers.auth_controller import register_user_controller, login_user_controller
from controllers.add_question_controller import add_question_controller
from controllers.show_multiple_choice_controller import show_multiple_choice_controller
# objectclass
from models.essay import Essay
from models.multiplechoice import MultipleChoiceQuestion
from models.users import UserRegistration, UserLogin
from models.smartquestion import SmartQuestion

# utilities
from utilities.authen_token import get_current_user



app = FastAPI()

# Set up CORS

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "https://www.smart-exam.com",
        "http://localhost:5001",  # Containerized React app
        "http://localhost:3000",  # React development server
        "http://192.168.0.102:3000",  # If accessing from another device in the network
        # Add any other origins as needed
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



# Load environment variables from .env file
load_dotenv()

# Set OpenAI API key
openai.api_key = os.environ.get("OPENAI_API_KEY")



# test get
@app.get("/")
def read_root():
    return {"Hello": "World"}

# register and login

@app.post("/register")
async def register(user: UserRegistration):
    return await register_user_controller(user)

@app.post("/login")
async def login(user: UserLogin):
    return await login_user_controller(user)

# smart exam function

# allow teach add question into database
@app.post("/add_question")
async def add_question(question: SmartQuestion, current_user: str = Depends(get_current_user)):
    return await add_question_controller(question, current_user)

# show question for users to choose
@app.get("/show_multiple_choice", response_model=List[SmartQuestion])
async def show_multiple_choice(current_user: str = Depends(get_current_user)):
    print("heloooooo")
    return await show_multiple_choice_controller(current_user)


# multiple choice question
@app.post("/multiple_choice")
async def multiple_choice(question: MultipleChoiceQuestion, current_user: str = Depends(get_current_user)):
    return await multiple_choice_controller(question, current_user)

# checking essay (grammar and meaning)
# using the question object in models
@app.post("/checking_essay")
async def checking_essay(essay: Essay, current_user: str = Depends(get_current_user)):
    
    return await checking_essay_controller(essay, current_user)