# repositories/question_repository.py

from db_connect.db_connect import create_connection
from typing import List, Tuple

def show_questions() -> List[Tuple]:
    conn = create_connection()
    cur = conn.cursor()

    cur.execute("SELECT * FROM smart_questions")
    questions = cur.fetchall()

    cur.close()
    conn.close()

    return questions
