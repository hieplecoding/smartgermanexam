
from db_connect.db_connect import create_connection

def add_question(information_text, question, answer_1, answer_2, answer_3):
    try:
        # Assuming create_connection is a function that returns a connection to the database
        conn = create_connection()
        cur = conn.cursor()

        # SQL command to insert a new row into the smart_questions table
        insert_query = """
        INSERT INTO smart_questions (information_text, question, answer_1, answer_2, answer_3) 
        VALUES (%s, %s, %s, %s, %s)
        """
        cur.execute(insert_query, (information_text, question, answer_1, answer_2, answer_3))

        # Commit the transaction
        conn.commit()

        # Close the cursor and connection
        cur.close()
        conn.close()

        return "Question added successfully", True  # Indicate success

    except Exception as e:
        # Log the exception, close the cursor and connection if needed, and return an error message
        print(f"An error occurred: {e}")
        if conn:
            if not cur.closed:
                cur.close()
            if not conn.closed:
                conn.close()
        return f"Failed to add question: {str(e)}", False  # Indicate failure


