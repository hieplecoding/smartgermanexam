# repositories/user_repository.py

# Assuming you are using psycopg2 or similar library for database operations

from db_connect.db_connect import create_connection

def create_user(username: str, hashed_password: str):
    conn = create_connection()
    cur = conn.cursor()
    cur.execute("INSERT INTO users (username, hashed_password) VALUES (%s, %s)", (username, hashed_password))
    conn.commit()
    cur.close()
    conn.close()

def get_user(username: str):
    try:
        conn = create_connection()
        cur = conn.cursor()
        cur.execute("SELECT * FROM users WHERE username = %s", (username,))
        user = cur.fetchone()
        cur.close()
        conn.close()
        return user
    except Exception as e:
        print(f"An error occurred: {e}")
        return None

