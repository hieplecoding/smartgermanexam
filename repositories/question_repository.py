# repositories/question_repository.py

from db_connect.db_connect import create_connection

def get_question_by_id(id_question):
    conn = create_connection()
    cur = conn.cursor()

    cur.execute("SELECT * FROM smart_questions WHERE id_question = %s", (id_question,))
    question_row = cur.fetchone()

    cur.close()
    conn.close()

    return question_row
