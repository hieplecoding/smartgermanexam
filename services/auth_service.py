# services/auth_service.py

from utilities.security import hash_password, verify_password
from repositories.user_repository import get_user, create_user
from utilities.token import create_access_token

def register_user(username: str, password: str):
    hashed_password = hash_password(password)
    # Create and save the new user
    create_user(username, hashed_password)
    return "User registered successfully"

def authenticate_user(username: str, password: str):
    user = get_user(username)
    if user:
        # Assuming 'hashed_password' is the second element in the tuple
        hashed_password = user[2]
        
        if verify_password(password, hashed_password):
            return create_access_token({"sub": username})
    return None

def token_check_user_exits(username: str) -> bool:
    
    user = get_user(username)
    return user is not None