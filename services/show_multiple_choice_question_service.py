# services/show_multiple_choice_question_service.py

from typing import List
from models.smartquestion import SmartQuestion
from repositories.show_question import show_questions

def process_show_multiple_choice_question() -> List[SmartQuestion]:
    question_rows = show_questions()

    questions = []
    for row in question_rows:
        question = SmartQuestion(
            id_question=row[0],
            information_text=row[1],  # Adjust indices based on your table structure
            question=row[2],
            answer_1=row[3],
            answer_2=row[4],
            answer_3=row[5]
        )
        questions.append(question)

    return questions
