#implement the methods
from repositories.add_question_repository import add_question

def process_add_question(information_text:str, question: str, answer_1: str, answer_2: str, answer_3: str):
    try:
        response = add_question(information_text, question, answer_1, answer_2, answer_3)
        return response, True
    except Exception as e:
        return f"An error occurred: {str(e)}", False


def process_if_teacher(username: str) -> bool:
    return username.startswith("teacher_")
