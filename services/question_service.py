# services/question_service.py

import openai
from repositories.question_repository import get_question_by_id

def process_multiple_choice_question(id_question, user_answer):
    question_row = get_question_by_id(id_question)

    if not question_row:
        return "Question not found", False

    # Extracting details from the question row
    db_text, db_question, option_a, option_b, option_c= question_row[1], question_row[2], question_row[3], question_row[4], question_row[5]
    
    # Constructing the prompt for OpenAI API
    prompt = (
        f"Frage: {db_text}\n"
        f"Frage: {db_question}\n"
        f"A: {option_a}\n"
        f"B: {option_b}\n"
        f"C: {option_c}\n"
        
        "Welche Option ist die richtige Antwort? (A, B, C)"
    )
    
    # Sending the prompt to OpenAI API
    response = openai.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "Du bist ein Deutschlehrer und du musst die Fragen sorgfältig lesen. Wenn die Lösung von Users falsch ist, musst du sie erklären.\n" + prompt},
            {"role": "user", "content": "Antwort: " + user_answer}
        ]
    )

    return response.choices[0].message, True
