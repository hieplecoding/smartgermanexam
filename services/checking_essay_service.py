import openai

def process_checking_essay(essay_content):
    try:
        # Constructing the prompt for OpenAI API
        prompt = (
            "Du bist ein Deutschlehrer und bekommst den folgenden Aufsatz von einem Schüler. Bitte überprüfe den Aufsatz sorgfältig auf Grammatikfehler und Klarheit der Aussage:\n\n"
            f"{essay_content}\n\n"
            "Bitte gib konstruktives Feedback zum Aufsatz."
        )

        # Sending the prompt to OpenAI API using the Completion endpoint
        response = openai.completions.create(
            model="text-davinci-003",
            prompt=prompt,
            max_tokens=1024  # Adjust max_tokens as needed
        )

        # Formatting the response
        formatted_response = response.choices[0].text.strip()
        formatted_response = formatted_response.replace('\n', ' ').replace('\\"', '**')

        return formatted_response, True
    except Exception as e:
        return str(e), False
