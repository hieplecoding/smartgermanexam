19.12:
- first day to start this project. 
- let's make the backend for the smart exam project: to work with openai api
- implement the question and multiple choice 
- create a basic database for question and their answer.
- ... 

20.12:
- implement the checking text method (grammar and meaning)
- implement the login and authentication using jwt token
- using token to verify and allow to use other methods
- think about host this backebd application

21.12:
- containerize our project and database on docker (work)
- create a REACT frontend project
- implement the login and register
- cors connection between frontend and backend (work)
- create table for question

22.12:
- allow teach to add question in database
- rule: username for teacher always teacher_...
- make the better frontend
- think about deploy and host

23.12:
- front end day
- return response for register and login
- make the question box
- make the add check essay box

25.12:
- update the backend to show in the frontend text, question and answers.
- update frontend to handle that
- design the frontend
- think about host and deploy

26-30.12
- learn how to host on aws
- testing host frontend and backend 

3.1:
- connection to frontend on s3 and database on rds

31.1:
- continue our journey
- let's make this app 
