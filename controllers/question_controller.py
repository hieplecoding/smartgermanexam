# controllers/question_controller.py

from fastapi import HTTPException, status
from services.question_service import process_multiple_choice_question
from services.auth_service import token_check_user_exits


async def multiple_choice_controller(question, current_user):
    user_exists = token_check_user_exits(current_user)
    
    if not user_exists:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    response, success = process_multiple_choice_question(question.id_question, question.user_answer)

    if not success:
        raise HTTPException(status_code=404, detail=response)

    return {"response": response}

