
from fastapi import HTTPException, status

from services.auth_service import token_check_user_exits
from services.show_multiple_choice_question_service import process_show_multiple_choice_question

async def show_multiple_choice_controller(current_user):
    user_exists = token_check_user_exits(current_user)
    print(user_exists)
    print(user_exists)
    if not user_exists:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found ***")

    questions = process_show_multiple_choice_question()
    if not questions:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No questions found ***")

    return questions
