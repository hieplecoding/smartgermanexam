# controllers/auth_controller.py

from fastapi import HTTPException, status
from models.users import UserRegistration, UserLogin
from services.auth_service import register_user, authenticate_user

async def register_user_controller(user: UserRegistration):
    return register_user(user.username, user.password)

async def login_user_controller(user: UserLogin):
    token = authenticate_user(user.username, user.password)
    if token:
        return {"access_token": token, "token_type": "bearer"}
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password"
        )
