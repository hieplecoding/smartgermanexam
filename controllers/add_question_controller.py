from fastapi import HTTPException, status
from services.add_question_service import process_add_question, process_if_teacher
from services.auth_service import token_check_user_exits
from models.smartquestion import SmartQuestion


async def add_question_controller(question: SmartQuestion, current_user):
    user_exists = token_check_user_exits(current_user)
    
    if not user_exists:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    
    # check if they are teacher
    # implement the function here
    #print(current_user)
    if not process_if_teacher(current_user):
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Only teacher can add question")

    response, success = process_add_question(question.information_text, question.question, question.answer_1, question.answer_2, question.answer_3)

    if not success:
        raise HTTPException(status_code=404, detail=response)

    return {"response": response}
