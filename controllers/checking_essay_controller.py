from fastapi import HTTPException, status
from services.checking_essay_service import process_checking_essay
from services.auth_service import token_check_user_exits

async def checking_essay_controller(essay, current_user):
    user_exists = token_check_user_exits(current_user)
    
    if not user_exists:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    response, success = process_checking_essay(essay.content)

    if not success:
        raise HTTPException(status_code=404, detail=response)

    return {"response": response}
