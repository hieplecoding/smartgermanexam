# models/question.py
from pydantic import BaseModel

class SmartQuestion(BaseModel):
    id_question: int
    information_text: str
    question: str
    answer_1: str
    answer_2: str
    answer_3: str
