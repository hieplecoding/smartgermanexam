from pydantic import BaseModel

class Essay(BaseModel):
    
    content: str
