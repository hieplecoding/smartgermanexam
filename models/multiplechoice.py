# models/question.py
from pydantic import BaseModel

class MultipleChoiceQuestion(BaseModel):
    id_question: int
    user_answer: str
